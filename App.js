import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import {MainView} from './components/MainView';
import { Header } from './components/common';
import {APP_COLORS} from './style/color';


export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <Header
        headerTitle="Simulateur d'impots"/>
        <MainView/>  
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: APP_COLORS.background || 'white',
  },
  header: {
    height: 140, 
  },
  
});
