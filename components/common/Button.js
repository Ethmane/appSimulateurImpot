
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

const Button = ({onPress, children}) => {
	return (
		<TouchableOpacity onPress={ () => onPress()}>
			<View style={styles.container}>
				<Text  style={styles.btn}>{children}</Text>
			</View>
		</TouchableOpacity>
	);
};

Button.propTypes = {
	onPress: PropTypes.func,
	children: PropTypes.string
};

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#8285d6',
		borderRadius: 20,
		justifyContent: 'center',
		alignItems: 'center'
	},
	btn: {
		color: 'white',
		fontSize: 16,
		padding: 4,
	},

});

export {Button};
