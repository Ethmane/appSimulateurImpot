import React from 'react';
import { View } from 'react-native';
import {APP_COLORS} from '../../style/color';
import PropTypes from 'prop-types';

const Card = (props) => {
	return (
		<View style={styles.containerStyle}>
			{props.children}
		</View>
	);
};

Card.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.array
	])
};

const styles = {
	containerStyle: {
		borderWidth: 1,
		borderRadius: 2,
		borderColor: APP_COLORS.lines || 'gray',
		borderBottomWidth: 0,
		shadowColor:  '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.1,
		shadowRadius: 2,
		elevation: 1,
		margin: '3%',
		backgroundColor: APP_COLORS.primary|| 'white',

	}
};

export {Card};